import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerComponent } from './customer/customer.component';
import { InsuranceCarComponent } from './insurance-car/insurance-car.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'customer',
  },
  {
    path: 'customer',
    component: CustomerComponent
  },
  {
    path: 'insurance',
    component: InsuranceCarComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

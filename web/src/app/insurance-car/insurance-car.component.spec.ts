import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsuranceCarComponent } from './insurance-car.component';

describe('InsuranceCarComponent', () => {
  let component: InsuranceCarComponent;
  let fixture: ComponentFixture<InsuranceCarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsuranceCarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsuranceCarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
